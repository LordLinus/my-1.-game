package model;

public class Projektil {
    // Eigenschaften
    private Double x;
    private Double y;
    private boolean isColliding = false;
    private int h;
    private int w;
    private float speedX;
    private float speedY;

    // Konstruktoren
    public Projektil(float speedX, float speedY, double x, double y) {
        this.h = 30;
        this.w = 30;
        this.speedX = speedX;
        this.speedY = speedY;
        this.x = x;
        this.y = y;
    }

    // Methoden
    public void move (double dx, double dy) {
        if(!this.isColliding) {
            this.x += dx;
            this.y += dy;
        }
        else {
            this.x = dx;
            this.y = dy;
        }
    }

    // Setter + Getter
    public double getX() {
        return this.x;
    }

    public double getY() {
        return this.y;
    }

    public int getH() {
        return this.h;
    }

    public int getW() {
        return this.w;
    }

    public float getSpeedX() {
        return this.speedX;
    }

    public float getSpeedY() {
        return this.speedY;
    }

    public boolean isColliding() {
        return isColliding;
    }

    public void setColliding(boolean colliding) {
        isColliding = colliding;
    }
}
