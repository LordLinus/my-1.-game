package model;

public class Turm {
    private int x;
    private int y;
    private int h;
    private int w;
    private int schaden;
    private int kosten;
    private int reichweite;
    //Neuer kram
//    double rotationLimitDeg=0;
//    double rotationLimitRad =  Math.toDegrees( this.rotationLimitDeg);
//    double roatationEasing = 10; // größere Zahl = kleinere rotation
//    double targetAngle = 0;
//    double currentAngle = 0;
//
//    boolean withinFiringRange = false;
//    double r = 0;


    public Turm(int x, int y, int schaden) {
        this.x = x;
        this.y = y;
        this.h = 60;
        this.w = 60;
        this.schaden = schaden;
        this.kosten = 100;
        reichweite = 250;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getH() {
        return h;
    }

    public int getW() {
        return w;
    }

    public int getKosten() {
        return kosten;
    }


    public void attack(Enemy enemy) {
        enemy.isDamaged(schaden);
    }

    public int getReichweite() {
        return reichweite;
    }
}
