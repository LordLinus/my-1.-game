package model;

public class Enemy {
    private int x;
    private int y;
    private int h;
    private int w;
    private int leben;
    private boolean schadengemacht;


    public Enemy(int x, int y) {
        this.x = x;
        this.y = y;
        this.h = 30;
        this.w = 30;
        this.leben = 4;
        this.schadengemacht = false;
    }
    public void move(int dx, int dy){
        this.x += dx;
        this.y += dy;
    }

    public void isDamaged(int damage) {
        leben = (leben - damage < 0) ? 0 : (leben - damage);
    }
    public boolean isDead() {
        return leben == 0;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getH() {
        return h;
    }

    public int getW() {
        return w;
    }

    public int getLeben() {
        return leben;
    }

    public boolean isSchadengemacht() {
        return schadengemacht;
    }

    public void setSchadengemacht(boolean schadengemacht) {
        this.schadengemacht = schadengemacht;
    }
}
