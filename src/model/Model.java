package model;

import javax.swing.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class Model {
    //Final
    public final double WIDTH = 800;
    public final double HEIGHT = 800;

    //Attribute / Eigenschaften
    private List<Projektil> bullets = new LinkedList<>();
    private List<Turm> türme = new ArrayList<>();
    private List<Enemy> enemies = new ArrayList<>();
    private List<Enemy> enemiesDead = new ArrayList<>();
    private Base festung = new Base(10, 200);
    private Player player;
    private int lp = festung.lebenspunkte;
    private long neueWelle = 10000;
    private long reload = 0;
    private long cleanupcrew = 500;
    private int rundencounter = 1;
    private Random random = new Random();
    private int target = 0;
    boolean zielvorhanden = false;

    private boolean changedPicture = false;

    //Konstruktoren
    public Model() {
        this.player = new Player(400, 700);
    }

    //Methoden
    public void update(long elapsedTime) {
        spawnProRunde(elapsedTime);
        try {
            angriffDerTürme(elapsedTime);
        }catch (IndexOutOfBoundsException e){
            System.out.print("");
        }
        if (zielvorhanden) {
            bulletMovingToTarget(bullets, enemies.get(target));
        }


        playerLpSchaden();
        player.move(0, 0);
    }

    public Base getFestung() {
        return festung;
    }

    public int towerDistance(Turm turm, Enemy enemy) {
        int distanceX = enemy.getX() - turm.getX();
        int distanceY = enemy.getY() - turm.getY();
        int distanceTotal = (int) Math.sqrt(distanceX * distanceX + distanceY * distanceY);
        return distanceTotal;
    }

    public void spawnTowers(int x, int y) {
        türme.add(new Turm(x, y, 1));
    }

    public void spawnEnemys() {
        for (int i = 0; i < 2; i++) {
            enemies.add(new Enemy(150 + random.nextInt(500), 10 + random.nextInt(200)));
        }
        festung.setGold(festung.getGold() + 25);
    }

    public boolean genugGeld() {
        if (festung.getGold() >= 100) {
            festung.setGold(festung.getGold() - 100);
            System.out.println(festung.getGold());
            return true;
        } else {
            return false;
        }
    }

    public boolean bauplatzcheck(int x, int y) {
        for (Turm turm : türme) {
            int z = x - turm.getX();
            int u = y - turm.getY();
            if (Math.abs(z) < 50 && Math.abs(u) < 50)
                return false;
        }
        return true;
    }

    public void spawnProRunde(long elapsedTime) {
        neueWelle -= elapsedTime;
        if (neueWelle <= 0) {
            spawnEnemys();
            neueWelle = 15000;
            for (int i = 0; i < rundencounter; i++) {
                spawnEnemys();
            }
            rundencounter++;
            if (rundencounter > 5) {
                rundencounter += 2;
            }
        }

        reload -= elapsedTime;
        cleanupcrew -= elapsedTime;
    }

    public void angriffDerTürme(long elapsedTime) throws IndexOutOfBoundsException {
        for (int i = 0; i < türme.size(); i++) {
            if (reload <= 0 && !(enemies.size() == 0)) {
                if (!zielvorhanden) {
                    if (enemies.size() == 0) {
                        zielvorhanden = false;

                    } else if (towerDistance(türme.get(i), enemies.get(target)) <= türme.get(i).getReichweite()) {
                        zielvorhanden = true;
                    } else if (target <= enemies.size()) {
                        target++;
                    }
                }
                if (enemies.get(target).isDead()) {
                    enemiesDead.add(enemies.get(target));
                    enemies.remove(target);
                    zielvorhanden = false;
                    if (changedPicture) {
                        //add zur neuen liste, mit neuem Bild TODO
                        bullets.remove(0);
                    }
                    if (cleanupcrew <= 0) {
                        enemiesDead.remove(target);
                    }
                }
                if (zielvorhanden) {
                    this.bullets.add(new Projektil(1.00f, 1.00f, türme.get(i).getX(), türme.get(i).getY()));
                    türme.get(i).attack(enemies.get(target));
                }
                target = 0;
            }
        }
        if (cleanupcrew <= elapsedTime) {
            cleanupcrew = 200;
        }
        if (reload <= elapsedTime) {
            reload = 80;
        }
    }

    public void bulletMovingToTarget(List<Projektil> bullets, Enemy target) {
        double speedMultiplier = 2.5d;
        for (Projektil projektil : bullets) {
            if (((target.getX() - projektil.getX()) >= 0) && ((target.getY() - projektil.getY()) >= 0)) {

                projektil.move(speedMultiplier * 2, speedMultiplier * 2);
            } else if (((target.getX() - projektil.getX()) <= 0) && ((target.getY() - projektil.getY()) <= 0)) {

                projektil.move(speedMultiplier * -2, speedMultiplier * -2);
            } else if (((target.getX() - projektil.getX()) <= 0) && ((target.getY() - projektil.getY()) >= 0)) {

                projektil.move(speedMultiplier * -2, speedMultiplier * 2);
            } else if (((target.getX() - projektil.getX()) >= 0) && ((target.getY() - projektil.getY()) <= 0)) {

                projektil.move(speedMultiplier * 2, speedMultiplier * -2);
            }
            if (((Math.abs((target.getX() - projektil.getX())) <= 2) && (Math.abs((target.getY() - projektil.getY())) <= 2))) {
                projektil.setColliding(true);
                changedPicture = true;
            }
        }
    }


    public void playerLpSchaden() {
        for (int i = 0; i < enemies.size(); i++) {
            enemies.get(i).move(0, 1);
            if (enemies.get(i).getY() > HEIGHT - 50 && !(enemies.get(i).isSchadengemacht())) {
                lp--;
                festung.setLebenspunkte(lp);
                enemies.get(i).setSchadengemacht(true);
                zielvorhanden = false;
                enemies.remove(enemies.get(i));
            }
        }
    }


    //Getter

    public List<Turm> getTürme() {
        return türme;
    }

    public Player getPlayer() {
        return player;
    }

    public List<Enemy> getEnemies() {
        return enemies;
    }

    public List<Enemy> getEnemiesDead() {
        return enemiesDead;
    }

    public boolean isChangedPicture() {
        return changedPicture;
    }

    public List<Projektil> getBullets() {
        return bullets;
    }
}
