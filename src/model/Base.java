package model;

public class Base {
    int lebenspunkte;
    int gold;
    boolean tot;

    public Base(int lebenspunkte, int gold) {
        this.lebenspunkte = lebenspunkte;
        this.gold = gold;
        this.tot = false;
    }

    public boolean isTot() {
        if (lebenspunkte <= 0){
        tot = true;
        }else {
            tot = false;
        }
        return tot;
    }

    public int getLebenspunkte() {
        return lebenspunkte;
    }

    public void setLebenspunkte(int lebenspunkte) {
        this.lebenspunkte = lebenspunkte;
    }

    public int getGold() {
        return gold;
    }

    public void setGold(int gold) {
        this.gold = gold;
    }
}
