import javafx.scene.canvas.GraphicsContext;

import javafx.scene.text.Font;
import model.*;
import javafx.scene.image.Image;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import javafx.scene.paint.Color;



public class Graphics {

    private GraphicsContext gc;
    private Model model;

    public Graphics(GraphicsContext gc, Model model) throws FileNotFoundException {
        this.gc = gc;
        this.model = model;
    }


    private Image image = new Image(new FileInputStream("src/Immages/player.png"));
    private Image imagePanzer = new Image(new FileInputStream("src/Immages/panzer.png"));
    private Image imagePanzerKaputt = new Image(new FileInputStream("src/Immages/panzerkaputt.png"));
    private Image imageTower = new Image(new FileInputStream("src/Immages/newtower.png"));
    private Image imageHintergrund = new Image(new FileInputStream("src/Immages/neuhintergrund.png"));
    private Image imageTot = new Image(new FileInputStream("src/Immages/gameover.png"));
    private Image imageprojektil = new Image(new FileInputStream("src/Immages/projektil.png"));


    public void draw() {

        gc.clearRect(0, 0, model.WIDTH, model.HEIGHT);
        gc.drawImage(imageHintergrund, 0, 0);
        gc.drawImage(image, model.getPlayer().getX(), model.getPlayer().getY(),
                model.getPlayer().getW(), model.getPlayer().getH());

        for (Enemy enemy : model.getEnemies()) {
            if (!enemy.isDead()) {
                gc.drawImage(imagePanzer, enemy.getX(), enemy.getY(),
                        enemy.getW(), enemy.getH());
            }
        }
        for (Enemy enemyDead : model.getEnemiesDead()) {

            gc.drawImage(imagePanzerKaputt, enemyDead.getX(), enemyDead.getY(),
                    enemyDead.getW(), enemyDead.getH());

        }

        for (Turm turm : model.getTürme()) {

            gc.drawImage(imageTower, turm.getX(), turm.getY(),
                    turm.getW(), turm.getH());
        }

        //HUD
        gc.setFill(Color.GOLD);
        gc.setFont(new Font("Arial", 20));
        gc.fillText("$:  " + model.getFestung().getGold(), 20, 30);

        gc.setFill(Color.RED);
        gc.setFont(new Font("Arial", 20));
        gc.fillText("LP:  " + model.getFestung().getLebenspunkte(), 20, 50);


        //Bullet normal
        for (Projektil projektil : model.getBullets()) {
            if (!model.isChangedPicture()) {
                gc.drawImage(imageprojektil, projektil.getX(), projektil.getY(),
                        projektil.getW(), projektil.getH());
            } else {
                gc.drawImage(imageprojektil, projektil.getX(), projektil.getY(),
                        projektil.getW(), projektil.getH());
            }
        }

        //TOT
        if (model.getFestung().isTot()) {
            gc.drawImage(imageTot, 0, 0);
        }
        //bullet if (changedpicture = true)
    }
}
